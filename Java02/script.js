"use strict";

// puntuaciones
const puntuaciones = [
  {
    equipo: "Toros Negros",
    puntos: [1, 3, 4, 2, 10, 8],
  },
  {
    equipo: "Amanecer Dorado",
    puntos: [8, 5, 2, 4, 7, 5, 3],
  },
  {
    equipo: "Águilas Plateadas",
    puntos: [5, 8, 3, 2, 5, 3],
  },
  {
    equipo: "Leones Carmesí",
    puntos: [5, 4, 3, 1, 2, 3, 4],
  },
  {
    equipo: "Rosas Azules",
    puntos: [2, 1, 3, 1, 4, 3, 4],
  },
  {
    equipo: "Mantis Verdes",
    puntos: [1, 4, 5, 1, 3],
  },
  {
    equipo: "Ciervos Celestes",
    puntos: [3, 5, 1, 1],
  },
  {
    equipo: "Pavos Reales Coral",
    puntos: [2, 3, 2, 1, 4, 3],
  },
  {
    equipo: "Orcas Moradas",
    puntos: [2, 3, 3, 4],
  },
];

/*const sumPuntos = puntuaciones
.filter((puntuacion) => {
  return puntuacion.puntos 
})

/*let sum = 0;

for (let i = 0; i < puntuaciones.length; i++) {
    sum += puntuaciones[i];
}
console.log(sum);*/

/*const sumar = (accumulador, curr) => accumulador + curr;
console.log(puntuaciones.reduce(sumar));*/

/*let sum =puntuaciones.sum(puntos)
console.log(sum)*/


puntuaciones.forEach(function(elemento){
  console.log(elemento)
})

/*const sumPuntos = puntuaciones
.map(item => item.puntos)
.reduce ((a , b) => a + b,0);

console.log(sumPuntos);*/

/*let sumPuntos = 0;
for (let punto of puntuaciones){
  sumPuntos += punto.puntos
}

console.log(sumPuntos);*/

const copiaPuntuaciones = [...puntuaciones];
const equiposA = copiaPuntuaciones
.sort ((a,b)=> a.puntos + b.puntos)
.reduce((equipos) => equipos.equipo)
